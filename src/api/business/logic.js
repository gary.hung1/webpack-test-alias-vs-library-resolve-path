const internal_userModel = require("~/gh-ns-model/user");
const library_userModel = require("gh-ns-model/user");

exports.run = function () {
  /* This is the git-submodule copy of the source code that lives inside our src folder */
  console.log(`internalUser abs path = ${internal_userModel.absPath()}`);

  /* This copy lives in the project's node_module folder */
  console.log(`libraryUser abs path = ${library_userModel.absPath()}`);
};
