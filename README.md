# Webpack Resolve Demonstration

This demo illustrates the `gh-ns-model` module are distinct separate copies when installed as both an NPM package and internally as a git-submodule.
After building, run the script at `dist/main.js` and the following results will show :

```
internalUser abs path = /Users/garyhung/source/experiment/webpack-test-alias-vs-library-resolve-path/src/gh-ns-model/user/index.js
libraryUser abs path = /Users/garyhung/source/experiment/webpack-test-alias-vs-library-resolve-path/node_modules/gh-ns-model/user/index.js
```

## Project Dependencies

```
nvm use 18
# babel-loader needs fs/promises dependency which is only found in node v14
```

Need to add a flag when cloning so submodules are also pulled down from their respective repo.

```
git clone --recurse-submodules [repo]
```

## BUILD

```
npm i
num run build
node dist/main.js
```

## Related Resources

```
# Submodule Repo
https://gitlab.com/gary.hung1/gh-ns-model

# NPM Package
https://www.npmjs.com/package/gh-ns-model
```

#### Notes

We performed a git submodule installation into the project and told git where to put the files:
**Remember** to use --recurse-submodules when cloning.
```
git submodule add git@gitlab.com:gary.hung1/gh-ns-model.git ./src/gh-ns-model
```

